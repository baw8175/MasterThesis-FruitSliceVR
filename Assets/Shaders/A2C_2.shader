Shader "Custom/Alpha To Coverage 2"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
		SubShader
	{
		Tags { "Queue" = "AlphaTest" "RenderType" = "TransparentCutout" }
		Cull Off

		Pass
		{
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "Lighting.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				half3 normal : NORMAL;
			};

			struct frag_in
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				half3 worldNormal : NORMAL;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _MainTex_TexelSize;

			frag_in vert(appdata v)
			{
				frag_in o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				return o;
			}

			fixed4 frag(frag_in i, fixed facing : VFACE) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);

				/* Calculate current mip level */
				float2 texture_coord = i.uv * _MainTex_TexelSize.zw;
				float2 dx = ddx(texture_coord);
				float2 dy = ddy(texture_coord);
				float MipLevel = max(0.0, 0.5 * log2(max(dot(dx, dx), dot(dy, dy))));
				col.a *= 1 + max(0, MipLevel) * 0.25;

				/* Sharpen texture alpha to the width of a pixel */
				col.a = (col.a - 0.5) / max(fwidth(col.a), 0.0001) + 0.5;
				clip(col.a - 0.5);

				/* Lighting calculations */
				half3 worldNormal = normalize(i.worldNormal * facing);
				fixed ndotl = saturate(dot(worldNormal, normalize(_WorldSpaceLightPos0.xyz)));
				col.rgb *= (ndotl * _LightColor0);

				return col;
			}
			ENDCG
		}
	}
}