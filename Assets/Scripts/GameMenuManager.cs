using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

public class GameMenuManager : MonoBehaviour
{
    [Header("UI Elements")]
    [SerializeField] private GameObject _menu;
    [SerializeField] private GameObject _endScreen;
    [SerializeField] private GameObject _controlsTutorialmenu;
    [SerializeField] private Button _startStandardButton = null;
    [SerializeField] private Button _nextSceneButton = null;

    [Header("Level State")]
    [SerializeField] private FruitThrower _fruitThrower;

    [Header("Timer")]
    [SerializeField] private Timer _timer;
    [SerializeField] private TMP_Text _timerText;
    [SerializeField] private float _duration = 300;

    [Header("Score")]
    [SerializeField] private ScoreManager _scoreManager;
    [SerializeField] private TMP_Text _scoreText;

    [Header("Tracking")]
    [SerializeField] private ControllerMovementTracker _movementTracker;
    [SerializeField] private SliceTracker _sliceTracker;

    [Header("XR Interactors")]
    [SerializeField] private XRRayInteractor _leftRayInteractor;
    [SerializeField] private XRRayInteractor _rightRayInteractor;

    private void Awake()
    {
        // don't show the timer and score
        _timerText.enabled = false;
        _scoreText.enabled = false;

        // adding a delegate with no parameters
        _startStandardButton.onClick.AddListener(OnStandardClick);

        // adding a delegate with no parameters
        _nextSceneButton.onClick.AddListener(OnNextSceneClick);
    }

    // triggered when the start standard button is clicked
    private void OnStandardClick()
    {
        // show and start the timer
        _timerText.enabled = true;
        _timer.StartTimer(_duration);

        // reset and show score
        _scoreManager.ModifyPoints(-_scoreManager.score);
        _scoreText.enabled = true;
        _scoreText.fontSize = 1f;

        // hide menu and ray interactors
        HideMainMenu();

        // reset tracked stats
        DataLogger._fruitsMissed = 0;
        DataLogger._bombsMissed = 0;
        _movementTracker.leftDistanceMoved = 0;
        _movementTracker.rightDistanceMoved = 0;
        _sliceTracker.amountOfFruitSliced = 0;
        _sliceTracker.amountOfBombsSliced = 0;
        _sliceTracker.amountOfFreezeBananasSliced = 0;
        _sliceTracker.amountOfFrenzyBananasSliced = 0;

        // start throwing fruit
        //fruitThrower.startThrowing((int)difficultySlider.value);
        _fruitThrower.StartThrowingRampUp();
    }

    // triggered when the return to main menu button is clicked
    private void OnNextSceneClick()
    {
        // ----- calculate next scene using the latin square -----

        // calculate index for latin square row based on pid
        int latinSquareIndex = (ParticipantManager.ParticipantId - 1) % LatinSquare.latinSquare.Length;

        // get the current scene name
        string currentSceneName = SceneManager.GetActiveScene().name;

        // find the current scene name in the latin square row
        int currentSceneIndex = System.Array.IndexOf(LatinSquare.latinSquare[latinSquareIndex], currentSceneName);

        // determine the next scene index
        string nextSceneName;
        if (currentSceneIndex == LatinSquare.latinSquare[latinSquareIndex].Length - 1) {
            nextSceneName = "EndScene";
        } else {
            nextSceneName = LatinSquare.latinSquare[latinSquareIndex][currentSceneIndex + 1];
        }

        // load that scene
        SceneManager.LoadScene(nextSceneName);
    }

    // shows end screen after a specified time delay
    public void ShowEndScreenAfterDelay(float delay)
    {
        Invoke("ShowEndScreen", delay);
    }

    // shows end screen UI
    private void ShowEndScreen()
    {
        // show the menus
        _endScreen.SetActive(true);
        _controlsTutorialmenu.SetActive(true);

        // hide the timer and score
        _timerText.enabled = false;
        _scoreText.enabled = false;

        // enable both ray interactors
        _leftRayInteractor.enabled = true;
        _rightRayInteractor.enabled = true;
    }

    // hides main menu UI
    private void HideMainMenu()
    {
        // don't show the menus anymore
        _menu.SetActive(false);
        _controlsTutorialmenu.SetActive(false);

        // disable both ray interactors
        _leftRayInteractor.enabled = false;
        _rightRayInteractor.enabled = false;
    }
}
