using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class EyeResolution : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private float _resolutionScale = 1.4f;

    private void Awake()
    {
        XRSettings.eyeTextureResolutionScale = _resolutionScale;
    }
}
