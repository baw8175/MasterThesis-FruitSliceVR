using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliceTracker : MonoBehaviour
{
    [HideInInspector] public int amountOfFruitSliced = 0;
    [HideInInspector] public int amountOfBombsSliced = 0;
    [HideInInspector] public int amountOfFreezeBananasSliced = 0;
    [HideInInspector] public int amountOfFrenzyBananasSliced = 0;
}
