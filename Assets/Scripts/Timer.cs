using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    [Header("UI Elements")]
    [SerializeField] private TMP_Text _timerText;

    [Header("Level State")]
    [SerializeField] private GameMenuManager _gameMenuManager;
    [SerializeField] private FruitThrower _fruitThrower;

    [Header("Score")]
    [SerializeField] private ScoreManager _scoreManager;
    [SerializeField] private TMP_Text _scoreText;

    [Header("Tracking")]
    [SerializeField] private ControllerMovementTracker _movementTracker;
    [SerializeField] private SliceTracker _sliceTracker;

    private float _timeRemaining;
    private bool _timerIsRunning = false;
    private string _startTime;

    // starts the timer
    public void StartTimer(float duration)
    {
        _timeRemaining = duration;
        _timerIsRunning = true;
        _startTime = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
    }

    private void Update()
    {
        if (_timerIsRunning)
        {
            if (_timeRemaining > 0)
            {
                _timeRemaining -= Time.deltaTime;
                DisplayTime(_timeRemaining);
            }
            else
            {
                _timeRemaining = 0;
                _timerIsRunning = false;

               // log data after 2 seconds (to also count fruit thrown at last second)
               Invoke("LogData", 2f);

                // stop throwing fruit
                _fruitThrower.StopThrowing();

                // increase score font size (up to 2) (after 3 seconds)
                //_scoreManager.Invoke("GrowScoreText", 3f);
                //scoreManager.GrowScoreTextAfterDelay(2f);

                // show main menu again (after 6 seconds)
                _gameMenuManager.ShowEndScreenAfterDelay(6f);
            }
        }
    }

    // log all data inside "Android/data/com.PatrickMohr.FruitSliceVR/files/"
    private void LogData()
    {
        string realismLevel = "";
        if (SceneManager.GetActiveScene().name == "JapaneseGarden_0")
        {
            realismLevel = "High";
        } else if (SceneManager.GetActiveScene().name == "JapaneseGarden_1")
        {
            realismLevel = "Medium";
        } else if (SceneManager.GetActiveScene().name == "JapaneseGarden_2")
        {
            realismLevel = "Low";
        }
        DataLogger.LogData(_startTime, realismLevel, _scoreManager.score, _sliceTracker.amountOfFruitSliced, _sliceTracker.amountOfBombsSliced, _movementTracker.leftDistanceMoved, _movementTracker.rightDistanceMoved);
    }

    // display the time
    private void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        _timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}