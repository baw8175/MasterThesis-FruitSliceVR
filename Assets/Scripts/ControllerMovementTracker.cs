using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerMovementTracker : MonoBehaviour
{
    [Header("Controllers")]
    [SerializeField] private Transform _leftController;
    [SerializeField] private Transform _rightController;

    private Vector3 _lastLeftPosition;
    private Vector3 _lastRightPosition;
    [HideInInspector] public float leftDistanceMoved;
    [HideInInspector] public float rightDistanceMoved;

    private void Start()
    {
        // set last positions
        _lastLeftPosition = _leftController.position;
        _lastRightPosition = _rightController.position;
    }

    private void Update()
    {
        // calculate distance moved since last update
        leftDistanceMoved += Vector3.Distance(_leftController.position, _lastLeftPosition);
        rightDistanceMoved += Vector3.Distance(_rightController.position, _lastRightPosition);

        // set last positions
        _lastLeftPosition = _leftController.position;
        _lastRightPosition = _rightController.position;
    }
}
