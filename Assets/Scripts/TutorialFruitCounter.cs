using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialFruitCounter : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] private TMP_Text _counterText;

    [Header("Level State")]
    [SerializeField] private TutorialMenuManager _tutorialMenuManager;
    [SerializeField] private FruitThrower _fruitThrower;

    [Header("Score")]
    [SerializeField] private ScoreManager _scoreManager;

    private bool _tutorialIsRunning = false;
    private int _fruitAmount;

    // starts the timer
    public void StartCounting(int amount)
    {
        _tutorialIsRunning = true;
        _fruitAmount = amount;
    }

    private void Update()
    {
        if (_tutorialIsRunning)
        {
            // display UI text indicating the progress
            _counterText.text = _scoreManager.score + "/3 Fruits Sliced";

            // if enough fruits have been sliced, end the tutorial
            if (_scoreManager.score >= _fruitAmount)
            {
                _tutorialIsRunning = false;

                // stop throwing fruit
                _fruitThrower.StopThrowing();

                // show bomb tutorial (after 4 seconds)
                _tutorialMenuManager.ShowBombTutorialAfterDelay(4f);
            }
        }
    }
}