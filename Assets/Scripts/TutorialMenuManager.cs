using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

public class TutorialMenuManager : MonoBehaviour
{
    [Header("UI Elements")]
    [SerializeField] private GameObject _menu;
    [SerializeField] private GameObject _tutorial;
    [SerializeField] private GameObject _bombTutorial;
    [SerializeField] private GameObject _controlsTutorialmenu;
    [SerializeField] private Button _tutorialButton = null;
    [SerializeField] private Button _startTutorialButton = null;
    [SerializeField] private Button _startGameButton = null;

    [Header("Level State")]
    [SerializeField] private FruitThrower _fruitThrower;

    [Header("Fruit Counter")]
    [SerializeField] private TutorialFruitCounter _fruitCounter;
    [SerializeField] private int _fruitsToSlice = 3;
    [SerializeField] private TMP_Text _counterText;

    [Header("Score")]
    [SerializeField] private ScoreManager _scoreManager;

    [Header("XR Interactors")]
    [SerializeField] private XRRayInteractor _leftRayInteractor;
    [SerializeField] private XRRayInteractor _rightRayInteractor;

    private void Awake()
    {
        // don't show the counter
        _counterText.enabled = false;

        // adding a delegate with no parameters
        _tutorialButton.onClick.AddListener(OnTutorialClick);

        // adding a delegate with no parameters
        _startTutorialButton.onClick.AddListener(OnStartTutorialClick);

        // adding a delegate with no parameters
        _startGameButton.onClick.AddListener(OnStartGameClick);
    }

    // triggered when the tutorial button is clicked
    private void OnTutorialClick()
    {
        // hide this menu
        _menu.SetActive(false);

        // show tutorial
        _tutorial.SetActive(true);
    }

    // triggered when the start tutorial button is clicked
    private void OnStartTutorialClick()
    {
        // reset score
        _scoreManager.ModifyPoints(-_scoreManager.score);

        // show and start fruit counter (counts fruits that need to be sliced)
        _counterText.enabled = true;
        _fruitCounter.StartCounting(_fruitsToSlice);

        // hide tutorial
        _tutorial.SetActive(false);

        // hide menu and ray interactors
        HideMainMenu();

        // start throwing fruit
        _fruitThrower.StartThrowingSingularFruits();
    }

    // triggered when the return to menu button is clicked
    private void OnStartGameClick()
    {
        // calculate next scene using the latin square
        int latinSquareIndex = (ParticipantManager.ParticipantId-1) % 6;
        string sceneName = LatinSquare.latinSquare[latinSquareIndex][0];

        // load that scene
        SceneManager.LoadScene(sceneName);
    }

    // shows bomb tutorial after a specified time delay
    public void ShowBombTutorialAfterDelay(float delay)
    {
        Invoke("ShowBombTutorial", delay);
    }

    // shows bomb tutorial UI
    private void ShowBombTutorial()
    {
        // hide counter UI
        _counterText.enabled = false;

        // show tutorial
        _bombTutorial.SetActive(true);

        // enable both ray interactors
        _leftRayInteractor.enabled = true;
        _rightRayInteractor.enabled = true;

        // throw 3 bombs after some delay
        _fruitThrower.ThrowSingularBombAfterDelay(1.5f);
        _fruitThrower.ThrowSingularBombAfterDelay(2f);
        _fruitThrower.ThrowSingularBombAfterDelay(2.5f);
    }

    // shows main menu after a specified time delay
    public void ShowMainMenuAfterDelay(float delay)
    {
        Invoke("ShowMainMenu", delay);
    }

    // shows main menu UI
    private void ShowMainMenu()
    {
        // show the menus
        _menu.SetActive(true);
        _controlsTutorialmenu.SetActive(true);

        // enable both ray interactors
        _leftRayInteractor.enabled = true;
        _rightRayInteractor.enabled = true;
    }

    // hides main menu UI
    private void HideMainMenu()
    {
        // don't show the menus anymore
        _menu.SetActive(false);
        _controlsTutorialmenu.SetActive(false);

        // disable both ray interactors
        _leftRayInteractor.enabled = false;
        _rightRayInteractor.enabled = false;
    }
}
