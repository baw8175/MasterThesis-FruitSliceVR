using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public static ObjectPool SharedInstance;

    public List<GameObject> pooledApples;
    public GameObject appleToPool;
    public int amountOfApplesToPool;

    private void Awake()
    {
        SharedInstance = this;
    }

    private void Start()
    {
        pooledApples = new List<GameObject>();
        GameObject tmp;
        for (int i = 0; i < amountOfApplesToPool; i++)
        {
            tmp = Instantiate(appleToPool);
            tmp.SetActive(false);
            pooledApples.Add(tmp);
        }
    }
}
