using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class ScoreManager : MonoBehaviour
{
    [HideInInspector] public static ScoreManager instance;

    [Header("Fruit Thrower")]
    [SerializeField] private FruitThrower _fruitThrower;

    [Header("Score")]
    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private float _maxSize = 2f;
    [SerializeField] private float _growSpeed = 0.2f;
    [SerializeField] public bool isGrowing = false;

    [Header("Combo")]
    [SerializeField] private TMP_Text _comboText;
    [SerializeField] private TMP_Text _comboTextPrefab;
    [SerializeField] private float _comboResetTime = 2f;
    [SerializeField] private float _comboTextDeleteTime = 2f;
    [SerializeField] private float _comboDisplayDuration = 4f;

    public int score = 0;
    private int comboCount = 0;
    private float lastScoreTime = 0;

    private void Awake() {
        instance = this;
    }

    private void Start()
    {
        _scoreText.text = score.ToString();

        _comboText.enabled = false;
    }

    private void Update()
    {
        // grow score text after standard round
        if (isGrowing)
        {
            // Increase font size gradually
            _scoreText.fontSize = Mathf.Min(_scoreText.fontSize + _growSpeed * Time.deltaTime, _maxSize);

            // Check if font size has reached the maximum size
            if (_scoreText.fontSize >= _maxSize)
            {
                isGrowing = false;
            }
        }

        // calculate time difference between this score and the last one
        float timeDifference = Time.time - lastScoreTime;

        // reset combo when time is over
        if (timeDifference > _comboResetTime)
        {
            if (comboCount >= 2)
            {
                // grant combo points (multiply all points during combo by combo number at the end => e.g. 4x: (1+1+1+1)*4 = 16 points )
                // (note that further below 1 point is always granted, therefore its "comboCount * (comboCount-1)")
                score += comboCount * (comboCount - 1);
                _scoreText.text = score.ToString();

                // display combo on rock
                DisplayCombo(_comboDisplayDuration);
            }

            // reset combo count
            comboCount = 0;
        }
    }

    // add one point to the score (for fruit slices)
    public void AddPoint()
    {
        // increment combo count
        comboCount++;

        // update last score time
        lastScoreTime = Time.time;

        // update score and display score text
        score += 1;
        _scoreText.text = score.ToString();
    }

    // modify the score without applying combo (for bombs)
    public void ModifyPoints(int amount)
    {
        // update score and score text
        score += amount;
        score = Mathf.Max(0, score);
        _scoreText.text = score.ToString();
    }

    // display combo for specified amount of time on rock
    private void DisplayCombo(float comboDisplayDuration)
    {
        // set combo text content and show it
        _comboText.text =  comboCount + "x";
        _comboText.enabled = true;

        // hide combo text after specified time
        CancelInvoke("HideCombo");
        Invoke("HideCombo", comboDisplayDuration);
    }

    // dont show the combo anymore
    private void HideCombo()
    {
        _comboText.enabled = false;
    }

    // spawn combo text prefab at position
    private void SpawnComboText(Vector3 position)
    {
        // create a new Canvas GameObject
        GameObject comboCanvasObject = new GameObject("ComboCanvas");
        Canvas comboCanvas = comboCanvasObject.AddComponent<Canvas>();
        comboCanvas.renderMode = RenderMode.WorldSpace;

        // instantiate combo text prefab
        TMP_Text comboText = Instantiate(_comboTextPrefab, position, Quaternion.identity);

        if (_fruitThrower.flipXZ)
        {
            comboText.transform.rotation = Quaternion.Euler(0, 90, 0);
        }

        // set the parent of the combo text to the Canvas
        comboText.transform.SetParent(comboCanvas.transform, false);

        // set combo text content
        comboText.text = "x" + comboCount;

        // destroy combo text after a certain time (e.g., 2 seconds)
        Destroy(comboText.gameObject, _comboTextDeleteTime);
        Destroy(comboCanvasObject, _comboTextDeleteTime);
    }

    // grow the score text
    private void GrowScoreText()
    {
        isGrowing = true;
    }

    // grow the score text after a specified time delay
    public void GrowScoreTextAfterDelay(float delay)
    {
        Invoke("GrowScoreText", delay);
    }
}
