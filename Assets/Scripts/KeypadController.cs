using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public class KeypadController : MonoBehaviour
{
    private List<int> _inputPasswordList = new List<int>();
    [SerializeField] private TMP_InputField _codeDisplay;
    [SerializeField] private Button _continueButton;

    private void Awake()
    {
        // disallow continuing without inputs
        _continueButton.interactable = false;
    }

    public void UserNumberEntry(int selectedNum)
    {

        if(_inputPasswordList.Count >= 2)
        {
            return;
        }

        _inputPasswordList.Add(selectedNum);

        // allow continuing
        _continueButton.interactable = true;

        UpdateDisplay();
    }

    // updates the display showing the current input
    private void UpdateDisplay()
    {
        _codeDisplay.text = null;

        // iterate through inputPasswordList and write everything to UI element
        for (int i = 0; i < _inputPasswordList.Count; i++)
        {
            _codeDisplay.text += _inputPasswordList[i];
        }
    }

    // delete the latest number input
    public void DeleteEntry()
    {
        if (_inputPasswordList.Count <= 0)
        {
            return;
        }

        _inputPasswordList.RemoveAt(_inputPasswordList.Count - 1);

        // disallow continuing without inputs
        if (_inputPasswordList.Count <= 0)
        {
            _continueButton.interactable = false;
        }

        UpdateDisplay();
    }

    // "continue" is pressed
    public void PressContinue()
    {
        int participantId = int.Parse(_codeDisplay.text);

        // save pid in static variable "ParticipantId"
        ParticipantManager.ProvideParticipantId(participantId);

        // determine scene to load (save in "_sceneToLoad")
        string realismLevel = DataLogger.GetLatestRealismLevelForParticipant(participantId);
        string sceneToLoad = "Tutorial";

        if (realismLevel == "High")
        {
            sceneToLoad = CalculateNextScene("JapaneseGarden_0");
        } else if (realismLevel == "Medium")
        {
            sceneToLoad = CalculateNextScene("JapaneseGarden_1");
        }
        else if (realismLevel == "Low")
        {
            sceneToLoad = CalculateNextScene("JapaneseGarden_2");
        } else
        {
            sceneToLoad = "Tutorial";
        }

        // load this scene
        SceneManager.LoadScene(sceneToLoad);
    }

    // calculate next scene using the latin square based on the last scene
    private string CalculateNextScene(string lastScene)
    {
        // calculate index for latin square row based on pid
        int latinSquareIndex = (ParticipantManager.ParticipantId - 1) % LatinSquare.latinSquare.Length;

        // find the current scene name in the latin square row
        int currentSceneIndex = System.Array.IndexOf(LatinSquare.latinSquare[latinSquareIndex], lastScene);

        // determine the next scene index
        string nextSceneName;
        if (currentSceneIndex == LatinSquare.latinSquare[latinSquareIndex].Length - 1)
        {
            nextSceneName = "EndScene";
        }
        else
        {
            nextSceneName = LatinSquare.latinSquare[latinSquareIndex][currentSceneIndex + 1];
        }

        return nextSceneName;
    }
}
