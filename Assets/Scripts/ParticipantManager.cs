using System.IO;
using UnityEngine;

// static class that holds the participant number data (initialized by "InitializeParticipant" in Tutorial Scene)
public static class ParticipantManager
{
    public static int ParticipantId { get; private set; }

    // set participant id and write it into "participantId.txt"
    public static void InitializeParticipantId()
    {
        string idFilePath = Path.Combine(Application.persistentDataPath, "participantId.txt");

        // load the pid from the file if it exists
        if (File.Exists(idFilePath))
        {
            string idStr = File.ReadAllText(idFilePath);
            if (int.TryParse(idStr, out int id))
            {
                ParticipantId = id + 1;  // increment for the new participant
            }
            else
            {
                ParticipantId = 1;  // default to 1 if file content is invalid
            }
        }
        else
        {
            ParticipantId = 1;  // default to 1 if file doesn't exist
        }

        // write pid to csv
        File.WriteAllText(idFilePath, ParticipantId.ToString());
    }

    // returns participantId
    public static void ProvideParticipantId(int id)
    {
        ParticipantId = id;
    }
}