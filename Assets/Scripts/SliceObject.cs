using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzySlice;
using UnityEngine.InputSystem;

public class SliceObject : MonoBehaviour
{

    [Header("Geometry")]
    [SerializeField] private Transform _startSlicePoint;
    [SerializeField] private Transform _endSlicePoint;
    [SerializeField] private VelocityEstimator _velocityEstimator;
    //[Tooltip("Required speed of the sword to slice fruit.")]
    //[SerializeField] private float _requiredSwordVelocity = 3f;
    private float _swordVelocity;
    private Vector3 _lastDirectionRef;
    private Vector3 _direction;
    private bool _canSlice;

    [Header("Rules")]
    [SerializeField] private LayerMask _sliceableLayer;
    [SerializeField] private bool _requireSliceAngle = false;

    [Header("Power Ups")]
    [SerializeField] private SlowMotionEffect _slowMotion;
    [SerializeField] private FruitThrower _fruitThrower;

    [Header("Visuals")]
    [SerializeField] private Material _crossSectionMaterial;
    [SerializeField] private float _cutForce = 2000;

    [Header("Audio")]
    [SerializeField] private AudioClip _squishAudioClip;
    [SerializeField] private float _squishPitch = 0.3f;
    [SerializeField] private AudioClip _sliceAudioClip;
    [SerializeField] private float _slicePitch = 0.8f;
    [SerializeField] private AudioClip _explosionAudioClip;
    [SerializeField] private float _explosionPitch = 0.8f;
    private AudioSource _audioSource;

    [Header("Particles")]
    [SerializeField] private ParticleSystem _pearParticles;
    [SerializeField] private ParticleSystem _appleParticles;
    [SerializeField] private ParticleSystem _orangeParticles;
    [SerializeField] private ParticleSystem _explosionParticles;
    [SerializeField] private ParticleSystem _smokeParticles;
    private ParticleSystem _fruitParticlesInstance;
    private ParticleSystem _explosionParticlesInstance;
    private ParticleSystem _smokeParticlesInstance;

    [Header("Tracking")]
    [SerializeField] private SliceTracker _sliceTracker;

    private void Start() {
        _audioSource = GetComponent<AudioSource>();
    }

    private void FixedUpdate() {

        // calculate slice direction and velocity
        _direction = transform.position - _lastDirectionRef;
        _lastDirectionRef = transform.position;
        _swordVelocity = _direction.magnitude / Time.deltaTime;
        _direction = _direction.normalized;
        if (!_requireSliceAngle || Vector3.Angle(_direction, -transform.up) <= 60)
        {
            _canSlice = true;
        }
        else
        {
            _canSlice = false;
        }

        // determine of fruit/bomb was hit
        bool hasHit = Physics.Linecast(_startSlicePoint.position, _endSlicePoint.position, out RaycastHit hit, _sliceableLayer);
        if (hasHit)
        {
            GameObject target = hit.transform.gameObject;

            // check if it was a bomb, banana, or regular fruit
            if (target.tag == "Bomb")
            {
                _sliceTracker.amountOfBombsSliced++;
                Explode(target);
            } else
            {
                // if it was a fruit, check whether to slice or bounce it
                if (_canSlice)
                {
                    // if it was a banana, also apply the correct bonus
                    if (target.tag == "FreezeBanana")
                    {
                        _sliceTracker.amountOfFreezeBananasSliced++;
                        _slowMotion.EnableSlowMotionEffect();
                    } else if (target.tag == "FrenzyBanana")
                    {
                        _sliceTracker.amountOfFrenzyBananasSliced++;
                        _fruitThrower.StartFrenzy(0.5f,2f);
                    }

                    Slice(target);
                }
                else
                {
                    Rigidbody fruitRB = target.GetComponent<Rigidbody>();
                    fruitRB.interpolation = RigidbodyInterpolation.Extrapolate;
                    Bounce(fruitRB, _direction, _swordVelocity);
                }
            }
        }
    }

    // make the bomb explode
    private void Explode(GameObject bomb)
    {
        // play sfx
        _audioSource.volume = 1f;
        _audioSource.pitch = _explosionPitch * Time.timeScale;
        _audioSource.PlayOneShot(_explosionAudioClip);

        _audioSource.volume = 0.4f;
        _audioSource.pitch = _slicePitch * Time.timeScale;
        _audioSource.PlayOneShot(_sliceAudioClip);


        // spawn particles
        _smokeParticlesInstance = Instantiate(_smokeParticles, bomb.transform.position, Quaternion.identity);
        _explosionParticlesInstance = Instantiate(_explosionParticles, bomb.transform.position, Quaternion.identity);

        // reduce score by 10
        ScoreManager.instance.ModifyPoints(-5);

        Destroy(bomb);
    }

    // slice the fruit
    public void Slice(GameObject target) {

        Vector3 velocity = _velocityEstimator.GetVelocityEstimate();
        Vector3 planeNormal = Vector3.Cross(_endSlicePoint.position - _startSlicePoint.position, velocity);
        planeNormal.Normalize();

        SlicedHull hull = target.Slice(_endSlicePoint.position, planeNormal);

        if (hull != null) {
            GameObject upperHull = hull.CreateUpperHull(target,_crossSectionMaterial);
            SetupSlicedComponent(upperHull);
            upperHull.name = target.name + "_upperHull";
            GameObject lowerHull = hull.CreateLowerHull(target,_crossSectionMaterial);
            SetupSlicedComponent(lowerHull);
            lowerHull.name = target.name + "_lowerHull";


            // play sfx
            _audioSource.volume = 0.4f;
            _audioSource.pitch = _squishPitch * Time.timeScale;
            _audioSource.PlayOneShot(_squishAudioClip);

            _audioSource.volume = 0.4f;
            _audioSource.pitch = _slicePitch * Time.timeScale;
            _audioSource.PlayOneShot(_sliceAudioClip);

            // spawn particles
            if (target.name.StartsWith("apple"))
            {
                _fruitParticlesInstance = Instantiate(_appleParticles, target.transform.position, Quaternion.identity);
            } else if (target.name.StartsWith("orange"))
            {
                _fruitParticlesInstance = Instantiate(_orangeParticles, target.transform.position, Quaternion.identity);
            } else
            {
                _fruitParticlesInstance = Instantiate(_pearParticles, target.transform.position, Quaternion.identity);
            }


            // set score in UI (only if first cut)
            if (target.tag == "WholeFruit") {
                ScoreManager.instance.AddPoint();

                // track fruit slice
                _sliceTracker.amountOfFruitSliced++;
            }

            Destroy(target);
            StartCoroutine(DestroyChilds(upperHull, lowerHull));
        }
    }

    // when the sword can't slice
    private void Bounce(Rigidbody fruitRB, Vector3 collisionNormal, float velocity)
    {
        float speed = fruitRB.velocity.magnitude;
        fruitRB.velocity = collisionNormal * Mathf.Max(speed, velocity / 4);
    }

    public void SetupSlicedComponent(GameObject slicedObject) {
        Rigidbody rb = slicedObject.AddComponent<Rigidbody>();
        MeshCollider collider = slicedObject.AddComponent<MeshCollider>();
        collider.convex = true;
        slicedObject.layer = LayerMask.NameToLayer("Sliceable");
        rb.AddExplosionForce(_cutForce, slicedObject.transform.position, 0);
    }

    IEnumerator DestroyChilds(GameObject upperHull, GameObject lowerHull){
        yield return new WaitForSeconds(2.0f);
        Destroy(upperHull);
        Destroy(lowerHull);
    }
}
