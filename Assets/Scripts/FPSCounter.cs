using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FPSCounter : MonoBehaviour
{
    [Header("Text Game Object")]
    [SerializeField] private TMP_Text _fpsText;

    private float _deltaTime;

    private void Update()
    {
        // calculate fps
        _deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;
        float fps = 1.0f / _deltaTime;

        // show fps in UI text
        _fpsText.text = Mathf.Ceil(fps).ToString() + " fps";
    }
}
