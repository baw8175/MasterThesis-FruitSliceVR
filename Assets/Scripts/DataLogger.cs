using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class DataLogger
{
    private static string _directoryPath;

    public static int _fruitsMissed = 0;
    public static int _bombsMissed = 0;

    public static void Start()
    {
        _directoryPath = Path.Combine(Application.persistentDataPath, "ParticipantData");

        // create the directory if it does not exist
        if (!Directory.Exists(_directoryPath))
        {
            Directory.CreateDirectory(_directoryPath);
        }
    }

    // gets the file path for a specific participant
    private static string GetFilePath(int participantId)
    {
        return Path.Combine(_directoryPath, $"Participant_{participantId}_GameData.csv");
    }

    // adds a new row to the csv with the specified data
    public static void LogData(string startTime, string realismLevel, int score, int fruitsSliced, int bombsHit, float leftControllerDistance, float rightControllerDistance)
    {
        int participantId = ParticipantManager.ParticipantId;
        string filePath = GetFilePath(participantId);

        // check if the file already exists and write headers if it doesn't
        if (!File.Exists(filePath))
        {
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("startTime,endTime,participantId,realismLevel,score,fruitsSliced,fruitsMissed,bombsHit,bombsMissed,leftControllerDistance,rightControllerDistance");
            }
        }

        using (StreamWriter writer = new StreamWriter(filePath, true))
        {
            string endTime = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string data = $"{startTime},{endTime},{participantId},{realismLevel},{score},{fruitsSliced},{_fruitsMissed},{bombsHit},{_bombsMissed},{leftControllerDistance},{rightControllerDistance}";
            writer.WriteLine(data);
        }
    }

    // reads the latest row for a certain participant id and returns the realism level if one exists, else return "NONE"
    public static string GetLatestRealismLevelForParticipant(int participantId)
    {
        string latestRealismLevel = "NONE";
        string filePath = GetFilePath(participantId);

        if (File.Exists(filePath))
        {
            string[] lines = File.ReadAllLines(filePath);

            // loop through the lines in reverse order to find the latest entry
            for (int i = lines.Length - 1; i >= 0; i--)
            {
                string[] columns = lines[i].Split(',');

                if (columns.Length > 2 && int.TryParse(columns[2], out int id) && id == participantId)
                {
                    latestRealismLevel = columns[3];
                    break;
                }
            }
        }

        return latestRealismLevel;
    }
}
