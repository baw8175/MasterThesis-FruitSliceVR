using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class FruitThrower : MonoBehaviour
{
    [HideInInspector] public static FruitThrower instance;

    [Header("Fruit")]
    [Tooltip("All fruit prefabs to be thrown.")]
    [SerializeField] private GameObject[] _fruitPrefabs;

    [Header("Power-Ups")]
    [SerializeField] private GameObject[] _bananaPrefabs;
    [SerializeField] private GameObject _freezeBananaPrefab;
    [SerializeField] private GameObject _frenzyBananaPrefab;
    [SerializeField] private bool _isFrenzyActive = false;

    [Header("Bomb")]
    [SerializeField] private GameObject _bombPrefab;

    [Header("Throwing")]
    [Tooltip("If fruit thrower is turned 90 degrees in map, set this to true.")]
    [SerializeField] public bool flipXZ = false;
    [Tooltip("Min amount of fruit per charge.")]
    [SerializeField] public int minFruitAmount = 2;
    [Tooltip("Max amount of fruit per charge.")]
    [SerializeField] public int maxFruitAmount = 4;
    [Tooltip("Min amount of bombs per charge.")]
    [SerializeField] public int minBombAmount = 0;
    [Tooltip("Max amount of bombs per charge.")]
    [SerializeField] public int maxBombAmount = 1;
    [Tooltip("The force at which the item is thrown.")]
    [SerializeField] private float _throwForce = 5f;
    [Tooltip("Maximum variance in throwing force (factor).")]
    [SerializeField] private float _throwForceVariance = 0.1f;
    [Tooltip("Time between throws.")]
    [SerializeField] private float _throwInterval = 0.2f;
    [Tooltip("Time after fruit is destroyed.")]
    [SerializeField] private float _destroyDelay = 2f;
    [Tooltip("The maximum torque to apply to the fruit.")]
    [SerializeField] private float _maxTorque = 0.01f;
    [Tooltip("The maximum sideways force to apply to the fruit.")]
    [SerializeField] private float _maxSidewaysForce = 0.5f;

    [Header("Debug")]
    [SerializeField] private bool _throwOnStart = false;
    [SerializeField] private bool _frenzyOnStart = false;

    // used to determine what to throw when
    private int _chargeCounter = 0;
    [HideInInspector] public float[,,] charges; // defined in "ChargePatterns"

    private AudioSource _audioSource;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();

        // debug
        if (_throwOnStart)
        {
            //startThrowing(5);
            StartThrowingRampUp();
        }
        if (_frenzyOnStart)
        {
            StartFrenzy(0.5f,4f);
        }
    }

    // throws fruit for 5 minutes with increasing difficulty and fixedly defined fruit, powerup and bomb amounts
    public void StartThrowingRampUp()
    {
        _chargeCounter = 0;

        // start throwing process
        InvokeRepeating("ThrowChargeRampUp", 2f, _throwInterval);
    }

    // throw a single charge of fruit and bombs with ramping up difficulty and fixedly defined parameters
    private void ThrowChargeRampUp()
    {
        // extract correct outer array from "ChargePatterns"
        float[,] chargeArray = new float[charges.GetLength(1), charges.GetLength(2)];
        for (int j = 0; j < charges.GetLength(1); j++)
        {
            for (int k = 0; k < charges.GetLength(2); k++)
            {
                chargeArray[j, k] = charges[_chargeCounter, j, k];
            }
        }

        // calculate the amount of entries where the first entry is not -1
        int itemAmount = 0;
        for (int j = 0; j < chargeArray.GetLength(0); j++)
        {
            if (chargeArray[j, 0] != -1)
            {
                itemAmount++;
            }
        }

        // iterate over chargeArray to throw the fruits and bombs
        int itemCounter = 0;
        for (int j = 0; j < chargeArray.GetLength(0); j++)
        {
            // check if the first entry of the inner array is not -1
            if (chargeArray[j, 0] != -1)
            {
                // gather throw information
                int type = (int)chargeArray[j, 0];
                float upforce = chargeArray[j, 1];
                float sideforce = chargeArray[j, 2];
                float torquex = chargeArray[j, 3];
                float torquey = chargeArray[j, 4];
                float torquez = chargeArray[j, 5];

                // throw bomb / fruit
                if (type == 3)
                {
                    // throw bomb
                    ThrowOneFixed(_bombPrefab, upforce, sideforce, torquex, torquey, torquez, itemCounter, itemAmount);
                    itemCounter++;
                } else
                {
                    // throw fruit
                    ThrowOneFixed(_fruitPrefabs[type], upforce, sideforce, torquex, torquey, torquez, itemCounter, itemAmount);
                    itemCounter++;
                }
            }
        }

        _chargeCounter++;
    }

    // start throwing singular fruits
    public void StartThrowingSingularFruits()
    {
        _chargeCounter = 0;

        // start throwing process
        InvokeRepeating("ThrowSingularFruit", 2f, _throwInterval);
    }

    // throw a single charge of a singular fruit
    private void ThrowSingularFruit()
    {
        // choose random fruit prefab from the array
        GameObject randomFruitPrefab = _fruitPrefabs[Random.Range(0, _fruitPrefabs.Length)];

        ThrowOne(randomFruitPrefab, 0, 1);
    }

    // throw a singular bomb after a specified delay
    public void ThrowSingularBombAfterDelay(float delay)
    {
        Invoke("ThrowSingularBomb", delay);
    }

    // throw a singular bomb
    private void ThrowSingularBomb()
    {
        ThrowOne(_bombPrefab, 0, 1);
    }

    // throws one GameObject
    private void ThrowOne(GameObject prefab, int itemIndex, int itemAmount)
    {
        // calculate position above plane
        Vector3 throwPosition = DetermineThrowPos(itemIndex, itemAmount);

        // play sound effect
        _audioSource.Play();

        // spawn object at this point
        GameObject thrownObject = Instantiate(prefab, throwPosition, prefab.transform.rotation);

        // apply force to object
        ApplyForce(thrownObject);

        // destroy the object after a delay
        Destroy(thrownObject, _destroyDelay);
    }

    // throws one GameObject with fully defined parameters
    private void ThrowOneFixed(GameObject prefab, float upforce, float sideforce, float torquex, float torquey, float torquez, int itemIndex, int itemAmount)
    {
        // calculate position above plane
        Vector3 throwPosition = DetermineThrowPos(itemIndex, itemAmount);

        // play sound effect
        _audioSource.Play();

        // spawn fruit at this point
        GameObject thrownObject = Instantiate(prefab, throwPosition, prefab.transform.rotation);

        // apply force to fruit
        ApplyFixedForce(thrownObject, upforce, sideforce, torquex, torquey, torquez);

        // destroy the fruit after a delay (now done via "AutoDestroy" in items themselves to track as missed)
        //Destroy(thrownObject, _destroyDelay);
    }

    // helper method to stop throwing
    public void StopThrowing()
    {
        StopFrenzyThrowing();
        CancelInvoke("ThrowCharge");
        CancelInvoke("ThrowSingularFruit");
        CancelInvoke("ThrowChargeRampUp");
    }

    // determine random throw position above throw plane
    private Vector3 RandomlyDetermineThrowPos()
    {
        float randomX;
        float randomZ;

        if (flipXZ)
        {
            randomX = Random.Range(-transform.localScale.z / 2f, transform.localScale.z / 2f);
            randomZ = Random.Range(-transform.localScale.x / 2f, transform.localScale.x / 2f);
        } else
        {
            randomX = Random.Range(-transform.localScale.x / 2f, transform.localScale.x / 2f);
            randomZ = Random.Range(-transform.localScale.z / 2f, transform.localScale.z / 2f);
        }

        return transform.position + new Vector3(randomX, 0.06f, randomZ);
    }

    // determine throw position depending on thrown item amount
    private Vector3 DetermineThrowPos(int itemIndex, int itemAmount)
    {
        Vector3 throwPosition = new Vector3(0, 0, 0);

        if (itemAmount == 1)
        {
            throwPosition = new Vector3(26.225f, 1.87f, 27.15f);
        }
        else if (itemAmount == 2)
        {
            if (itemIndex == 0)
            {
                throwPosition = new Vector3(26.042f, 1.87f, 27.15f);
            }
            else
            {
                throwPosition = new Vector3(26.419f, 1.87f, 27.15f);
            }
        }
        else if (itemAmount == 3)
        {
            if (itemIndex == 0)
            {
                throwPosition = new Vector3(25.854f, 1.87f, 27.15f);
            }
            else if (itemIndex == 1)
            {
                throwPosition = new Vector3(26.225f, 1.87f, 27.15f);
            }
            else
            {
                throwPosition = new Vector3(26.597f, 1.87f, 27.15f);
            }
        }
        else if (itemAmount == 4)
        {
            if (itemIndex == 0)
            {
                throwPosition = new Vector3(25.873f, 1.87f, 27.224f);
            }
            else if (itemIndex == 1)
            {
                throwPosition = new Vector3(26.334f, 1.87f, 27.224f);
            }
            else if (itemIndex == 2)
            {
                throwPosition = new Vector3(26.092f, 1.87f, 27.016f);
            }
            else
            {
                throwPosition = new Vector3(26.561f, 1.87f, 27.016f);
            }
        }
        else if (itemAmount == 5)
        {
            if (itemIndex == 0)
            {
                throwPosition = new Vector3(25.852f, 1.87f, 27.016f);
            }
            else if (itemIndex == 1)
            {
                throwPosition = new Vector3(26.033f, 1.87f, 27.224f);
            }
            else if (itemIndex == 2)
            {
                throwPosition = new Vector3(26.393f, 1.87f, 27.224f);
            }
            else if (itemIndex == 3)
            {
                throwPosition = new Vector3(26.225f, 1.87f, 27.016f);
            }
            else
            {
                throwPosition = new Vector3(26.591f, 1.87f, 27.016f);
            }
        }
        else if (itemAmount == 6)
        {
            if (itemIndex == 0)
            {
                throwPosition = new Vector3(25.833f, 1.87f, 27.016f);
            }
            else if (itemIndex == 1)
            {
                throwPosition = new Vector3(25.973f, 1.87f, 27.224f);
            }
            else if (itemIndex == 2)
            {
                throwPosition = new Vector3(26.301f, 1.87f, 27.224f);
            }
            else if (itemIndex == 3)
            {
                throwPosition = new Vector3(26.589f, 1.87f, 27.224f);
            }
            else if (itemIndex == 4)
            {
                throwPosition = new Vector3(26.144f, 1.87f, 27.016f);
            }
            else
            {
                throwPosition = new Vector3(26.462f, 1.87f, 27.016f);
            }
        } else
        {
            throwPosition = RandomlyDetermineThrowPos();
        }

        return throwPosition;
    }

    // applies force to a thrown object
    private void ApplyForce(GameObject thrownObject)
    {
        // apply force to fruit
        Rigidbody fruitRb = thrownObject.GetComponent<Rigidbody>();
        if (fruitRb != null)
        {
            // upwards force
            fruitRb.AddForce(Vector3.up * _throwForce * Random.Range(1f - _throwForceVariance, 1f + _throwForceVariance), ForceMode.Impulse);

            // random rotational force
            float torqueX = Random.Range(-_maxTorque, _maxTorque);
            float torqueY = Random.Range(-_maxTorque, _maxTorque);
            float torqueZ = Random.Range(-_maxTorque, _maxTorque);
            fruitRb.AddTorque(new Vector3(torqueX, torqueY, torqueZ), ForceMode.Impulse);

            // random sideways force
            float sidewaysForce = Random.Range(-_maxSidewaysForce, _maxSidewaysForce);

            if (flipXZ)
            {
                fruitRb.AddForce(Vector3.forward * sidewaysForce, ForceMode.Impulse);
            } else {
                fruitRb.AddForce(Vector3.right * sidewaysForce, ForceMode.Impulse);
            }
        }
    }

    // applies force to a thrown object with fixed parameters
    private void ApplyFixedForce(GameObject thrownObject, float upforce, float sideforce, float torquex, float torquey, float torquez)
    {
        // apply force to fruit
        Rigidbody fruitRb = thrownObject.GetComponent<Rigidbody>();
        if (fruitRb != null)
        {
            // upwards force
            fruitRb.AddForce(Vector3.up * upforce, ForceMode.Impulse);

            // rotational force
            fruitRb.AddTorque(new Vector3(torquex, torquey, torquez), ForceMode.Impulse);

            // sideways force
            if (flipXZ)
            {
                fruitRb.AddForce(Vector3.forward * sideforce * 0.9f, ForceMode.Impulse);
            }
            else
            {
                fruitRb.AddForce(Vector3.right * sideforce * 0.9f, ForceMode.Impulse);
            }
        }
    }

    // used for the frenzy power up
    public void StartFrenzy(float frenzyThrowInterval, float duration)
    {
        _isFrenzyActive = true;

        // first, stop regular throwing
        CancelInvoke("ThrowChargeRampUp");
        CancelInvoke("ThrowSingularFruit");
        CancelInvoke("ThrowCharge");

        // start frenzy throwing
        Invoke("StopFrenzyThrowing", duration);
        InvokeRepeating("ThrowFrenzyCharge", 0f, frenzyThrowInterval);

        // then, start regular throwing again
        //InvokeRepeating("ThrowCharge", 1f, throwInterval);
        InvokeRepeating("ThrowChargeRampUp", 1f, _throwInterval);
    }

    // helper to start frenzy throws (needs to be different method so both can be cancelled individually)
    private void ThrowFrenzyCharge()
    {
        // set amount of fruit thrown
        int fruitAmount = 3;

        // throw fruit
        for (int i = 0; i < fruitAmount; i++)
        {
            // choose random fruit prefab from the array
            GameObject randomFruitPrefab = _fruitPrefabs[Random.Range(0, _fruitPrefabs.Length)];

            ThrowOne(randomFruitPrefab, i, fruitAmount);
        }
    }

    // helper method to stop frenzy throwing
    public void StopFrenzyThrowing()
    {
        _isFrenzyActive = false;
        CancelInvoke("ThrowFrenzyCharge");
    }
}