using UnityEngine;

public class SlowMotionEffect : MonoBehaviour
{
    [Header("Slow-Motion Settings")]
    [SerializeField] private float _slowMotionTimeScale = 0.5f;
    [SerializeField] private float _slowMotionDuration = 5.0f;

    private bool _slowMotionEnabled = false;
    private float _slowMotionTimer = 0f;
    private AudioSource _audioSource;

    [System.Serializable]
    public class AudioSourceData
    {
        public AudioSource audioSource;
        public float defaultPitch;
    }

    AudioSourceData[] audioSources;

    private void Start()
    {
        // audio source of power up manager
        _audioSource = GetComponent<AudioSource>();

        // find all AudioSources in the Scene and save their default pitch values
        AudioSource[] audios = FindObjectsOfType<AudioSource>();
        audioSources = new AudioSourceData[audios.Length];

        for (int i = 0; i < audios.Length; i++)
        {
            AudioSourceData tmpData = new AudioSourceData();
            tmpData.audioSource = audios[i];
            tmpData.defaultPitch = audios[i].pitch;
            audioSources[i] = tmpData;
        }
    }

    private void Update()
    {
        if (_slowMotionEnabled)
        {
            _slowMotionTimer -= Time.deltaTime;
            if (_slowMotionTimer <= 0f)
            {
                _slowMotionEnabled = false;
                DisableSlowMotionEffect();
            }
        }
    }

    // trigger the slow motion
    public void EnableSlowMotionEffect()
    {
        
        _slowMotionEnabled = true;
        Time.timeScale = _slowMotionTimeScale;
        _slowMotionTimer = _slowMotionDuration;

        // play sfx
        _audioSource.Play();

        ApplySoundChanges();
    }

    // disable the slow motion
    public void DisableSlowMotionEffect()
    {
        _slowMotionEnabled = false;
        Time.timeScale = 1f;

        ApplySoundChanges();
    }

    // apply sound changes (object slice sfx are managed in SliceObject)
    private void ApplySoundChanges()
    {
        for (int i = 0; i < audioSources.Length; i++)
        {
            if (audioSources[i].audioSource && audioSources[i].audioSource.clip.name != "slow_motion3")
            {
                audioSources[i].audioSource.pitch = audioSources[i].defaultPitch * Time.timeScale;
            }
        }
    }
}