using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    [Header("UI Elements")]
    [SerializeField] private Button _startLowRealismButton = null;
    [SerializeField] private Button _startMediumRealismButton = null;
    [SerializeField] private Button _startHighRealismButton = null;
    [SerializeField] private Button _startTutorialButton = null;

    [Header("XR Interactors")]
    [SerializeField] private XRRayInteractor _leftRayInteractor;
    [SerializeField] private XRRayInteractor _rightRayInteractor;

    private void Awake()
    {
        // adding a delegate with no parameters
        _startLowRealismButton.onClick.AddListener(OnLowRealismClick);

        // adding a delegate with no parameters
        _startMediumRealismButton.onClick.AddListener(OnMediumRealismClick);

        // adding a delegate with no parameters
        _startHighRealismButton.onClick.AddListener(OnHighRealismClick);

        // adding a delegate with no parameters
        _startTutorialButton.onClick.AddListener(OnTutorialClick);
    }

    // triggered when the low realism button is clicked
    private void OnLowRealismClick()
    {
        SceneManager.LoadScene("JapaneseGarden_2");
    }

    // triggered when the medium realism button is clicked
    private void OnMediumRealismClick()
    {
        SceneManager.LoadScene("JapaneseGarden_1");
    }

    // triggered when the high realism button is clicked
    private void OnHighRealismClick()
    {
        SceneManager.LoadScene("JapaneseGarden_0");
    }

    // triggered when the tutorial button is clicked
    private void OnTutorialClick()
    {
        SceneManager.LoadScene("Tutorial");
    }
}
