using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

// static class that holds the latin square data
public static class LatinSquare
{
    public static string[][] latinSquare = new string[][]
    {
        new string[] { "JapaneseGarden_2", "JapaneseGarden_1", "JapaneseGarden_0" },
        new string[] { "JapaneseGarden_2", "JapaneseGarden_0", "JapaneseGarden_1" },
        new string[] { "JapaneseGarden_0", "JapaneseGarden_2", "JapaneseGarden_1" },
        new string[] { "JapaneseGarden_0", "JapaneseGarden_1", "JapaneseGarden_2" },
        new string[] { "JapaneseGarden_1", "JapaneseGarden_0", "JapaneseGarden_2" },
        new string[] { "JapaneseGarden_1", "JapaneseGarden_2", "JapaneseGarden_0" },
    };
}