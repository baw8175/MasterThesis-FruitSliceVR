using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
    // time in seconds before the object is destroyed
    public float _destroyDelay = 2.8f;

    void Start()
    {
        Invoke("LogMissedAndDestroy", _destroyDelay);
    }

    // logs the game object as missed and destroys it
    private void LogMissedAndDestroy()
    {
        if (gameObject.name.StartsWith("bomb"))
        {
            DataLogger._bombsMissed += 1;
        } else
        {
            DataLogger._fruitsMissed += 1;
        }

        Destroy(gameObject);
    }
}
