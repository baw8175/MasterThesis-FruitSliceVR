using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshSimplifier : MonoBehaviour
{
    [Tooltip("The quality of the resulting mesh.")]
    [SerializeField] private float quality = 0.2f;

    private void Start()
    {
        // Ensure there is a MeshFilter component
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            Debug.LogError("No MeshFilter found on " + gameObject.name);
            return;
        }

        // Get the original mesh from the MeshFilter
        var originalMesh = meshFilter.sharedMesh;
        if (originalMesh == null)
        {
            Debug.LogError("No mesh found on " + gameObject.name);
            return;
        }

        // Check if the original mesh is readable
        if (!originalMesh.isReadable)
        {
            Debug.LogError("Original mesh is not readable. Please enable Read/Write in the import settings for " + originalMesh.name);
            return;
        }

        // Create a new mesh instance to avoid modifying the shared mesh
        var newMesh = Instantiate(originalMesh);
        newMesh.name = originalMesh.name + "_Simplified";

        // Ensure the new mesh has Read/Write enabled by marking it dynamic
        newMesh.MarkDynamic();

        // Check if the new mesh is readable
        if (!newMesh.isReadable)
        {
            Debug.LogError("New mesh is not readable after instantiation for " + gameObject.name);
            return;
        }

        // Use the simplifier on the new mesh instance
        var meshSimplifier = new UnityMeshSimplifier.MeshSimplifier();
        meshSimplifier.Initialize(newMesh);
        meshSimplifier.SimplifyMesh(quality);

        // Apply the simplified mesh to the MeshFilter
        meshFilter.sharedMesh = meshSimplifier.ToMesh();

        // Verify that the mesh simplification was successful
        if (meshFilter.sharedMesh == null)
        {
            Debug.LogError("Mesh simplification failed for " + gameObject.name);
        }
        else
        {
            Debug.Log("Mesh simplification succeeded for " + gameObject.name);
        }
    }
}
